import Vue from 'vue';
import Router from 'vue-router';
import Hello from '@/components/Hello';
import Variants from '@/components/Variants';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello,
    },
    {
      path: '/variants',
      name: 'Variants',
      component: Variants,
    },
  ],
  mode: 'history',
});
